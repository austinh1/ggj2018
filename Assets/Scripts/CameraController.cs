﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	private Vector3 cameraDistance;
	private Vector3 playerStartPosition;
	public Transform playerTransform;
	float rotationSpeed = 3f;
	float xLerpSpeed = 3f;

	// Use this for initialization
	void Start () {
		playerStartPosition = playerTransform.position;
		cameraDistance = playerStartPosition - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

	void FixedUpdate(){
		var targetPosition = new Vector3(playerTransform.position.x, playerStartPosition.y, playerTransform.position.z) - cameraDistance;
		transform.position = new Vector3(Mathf.Lerp(transform.position.x, targetPosition.x, xLerpSpeed * Time.deltaTime), targetPosition.y, Mathf.Lerp(transform.position.z, targetPosition.z, 1f));
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(playerTransform.position - transform.position), rotationSpeed * Time.deltaTime);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    Rigidbody body;
    public float forceAmount;
    bool knockedAway = false;

    public bool KnockedAway { get { return knockedAway; } }

	void Start () {
        body = GetComponent<Rigidbody>();
	}
	
	public void KnockAway(bool knockRight)
    {
        if(!knockedAway)
        {
            knockedAway = true;
            var sign = 1;
            if (!knockRight)
                sign = -1;

            body.AddForce(new Vector3(sign * 2, 1, 1f) * forceAmount, ForceMode.Impulse);
        }
    }
}

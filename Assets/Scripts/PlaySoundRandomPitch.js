
var minPitch = 1.0; //minimum pitch
var maxPitch = 2.0; //maximum pitch
function PlayRandomPitchSound (clip : AudioClip) {
	var randomPitch = (Random.Range(minPitch,maxPitch)); //sets random pitch range between min and max pitch above
	GetComponent.<AudioSource>().pitch = randomPitch;
	GetComponent.<AudioSource>().PlayOneShot(clip); //plays the clip at random pitch once, then destroys the audio source
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntersectionCar : MonoBehaviour
{
    Transform intersectionVertical;
    Transform intersectionHorizontal;
    public string intersectionVerticalName;
    public string intersectionHorizontalName;
    public GameObject carBody;
    public GameObject frontWheel;
    public GameObject backWheel;
    public GameObject carFront;
    public float speed;
    public Vector3 loopPosition;
    public Collider sideBottom;
    public Collider sideTop;
    public Collider front; 

    Rigidbody body;
    Obstacle obstacle;

    IntersectionState state; 

    void Start ()
    {
        state = IntersectionState.Left;
        body = GetComponent<Rigidbody>();
        obstacle = GetComponent<Obstacle>();

        intersectionVertical = GameObject.Find(intersectionVerticalName).transform;
        intersectionHorizontal = GameObject.Find(intersectionHorizontalName).transform;
    }

    void Update ()
    {
        if(!obstacle.KnockedAway)
        {
            if (state == IntersectionState.Left)
            {
                carBody.SetActive(true);
                frontWheel.SetActive(true);
                backWheel.SetActive(true);
                carFront.SetActive(false);

                sideBottom.enabled = true;
                sideTop.enabled = true;
                front.enabled = false;

                body.velocity = new Vector3(-speed, body.velocity.y, body.velocity.z);
                frontWheel.transform.Rotate(new Vector3(0, 0, 1f * speed));
                backWheel.transform.Rotate(new Vector3(0, 0, 1f * speed));

                if (transform.position.x < intersectionVertical.position.x)
                {
                    body.velocity = Vector3.zero;
                    state = IntersectionState.Down;
                }
            }
            else if (state == IntersectionState.Down)
            {
                carBody.SetActive(false);
                frontWheel.SetActive(false);
                backWheel.SetActive(false);
                carFront.SetActive(true);

                sideBottom.enabled = false;
                sideTop.enabled = false;
                front.enabled = true;

                body.velocity = new Vector3(body.velocity.x, body.velocity.y, -speed);

                if (transform.position.z < intersectionHorizontal.position.z)
                {
                    body.velocity = Vector3.zero;
                    state = IntersectionState.Right;
                }
            }
            else if (state == IntersectionState.Right)
            {
                carBody.SetActive(true);
                frontWheel.SetActive(true);
                backWheel.SetActive(true);
                carFront.SetActive(false);

                sideBottom.enabled = true;
                sideTop.enabled = true;
                front.enabled = false;

                body.velocity = new Vector3(speed, body.velocity.y, body.velocity.z);
                frontWheel.transform.Rotate(new Vector3(0, 0, 1f * speed));
                backWheel.transform.Rotate(new Vector3(0, 0, 1f * speed));

                if (transform.localPosition.x > loopPosition.x)
                {
                    transform.localPosition = loopPosition;
                    state = IntersectionState.Left;
                }
            }
        }
    }

    enum IntersectionState
    {
        Left,
        Down,
        Right
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pigeon : MonoBehaviour {

	SpriteRenderer spriteRenderer;

    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
    public Sprite sprite4;
    public float loopPosition;
    public float speed;
    private Vector3 startPosition;
    public int direction;
    float spriteSwitchTimer = 0.0f;
    float spriteSwitchTime = 0.25f;

    Rigidbody body;

    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
		startPosition = transform.position;
        body = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void Update () {

        if(spriteSwitchTimer < 0f)
        {
            SpriteChange();
            spriteSwitchTimer = spriteSwitchTime;
        }

        if (Mathf.Abs(transform.position.x) > loopPosition) {
            direction = direction * -1;
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        body.velocity = new Vector3( direction * speed, body.velocity.y, body.velocity.z);

        spriteSwitchTimer -= Time.deltaTime;
	}

    void SpriteChange()
    {
        if (spriteRenderer.sprite == sprite1)
        {
            spriteRenderer.sprite = sprite2;
        }
        if (spriteRenderer.sprite == sprite2)
        {
            spriteRenderer.sprite = sprite3;
        }
        if (spriteRenderer.sprite == sprite3)
        {
            spriteRenderer.sprite = sprite4;
        }
        else
        {
            spriteRenderer.sprite = sprite1;
        }

    }
}

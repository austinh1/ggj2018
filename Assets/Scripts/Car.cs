﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public bool goingLeft;
    public float loopPosition;
    public float speed;
    private GameObject frontWheel;
    private GameObject backWheel;

    Rigidbody body;

	void Start ()
    {
        body = GetComponent<Rigidbody>();
        frontWheel = transform.Find("Front Wheel").gameObject;
        backWheel = transform.Find("Back Wheel").gameObject;

	}
	
	void Update ()
    {
        var sign = goingLeft ? -1 : 1;

        body.velocity = new Vector3(sign * speed, body.velocity.y, body.velocity.z);
        frontWheel.transform.Rotate(new Vector3(0,0,1f*speed));
        backWheel.transform.Rotate(new Vector3(0,0,1f*speed));

        if(transform.position.x < -loopPosition)
            transform.position = new Vector3(loopPosition, transform.position.y, transform.position.z);
        else if (transform.position.x > loopPosition)
            transform.position = new Vector3(-loopPosition, transform.position.y, transform.position.z);
    }
}

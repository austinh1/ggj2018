﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour {
    public Sprite normal;
    public Sprite scared;

    Obstacle obstacle;
    SpriteRenderer spriteRenderer;

	void Start ()
    {
        obstacle = GetComponent<Obstacle>();
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	void Update ()
    {
        spriteRenderer.sprite = obstacle.KnockedAway ? scared : normal;
	}
}

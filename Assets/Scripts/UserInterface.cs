﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour {
    public Text heldMail;
    public Text deliveredMail;
    public Player player;

    void Start ()
    {
		
	}
	
	void Update ()
    {
        heldMail.text = player.HeldMail.ToString();
        deliveredMail.text = player.DeliveredMail.ToString();
    }
}

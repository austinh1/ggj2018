﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    Rigidbody body;
    Rewired.Player playerInput;
    Collider collider;
    AudioSource audioSource;

    float normalForwardSpeed = 7f;
    float normalForwardSpeedFaster = 8f;
    float normalForwardSpeedFastest = 9f;
    float dashForwardSpeed = 13f;
    float slowForwardSpeed = 3f;
    float horizontalSpeed = 5f;
    float jumpSpeed = 12f;
    float flapJumpSpeed = 7f;
    int maxJumpCount = 4;
    float invicibilityTime = 1f;
    float dashTime = .75f;
    float stumbleTime = 1f;

    float horizontalMovement;
    bool pressingJump;
    bool pressingDash;
    bool holdingSlow;
    bool pressingQuit;
    bool pressingRestart;

    int jumpCount = 0;
    bool grounded = false;
    float currentSpeed;
    float lerpToSpeed;
    float invicibilityTimer = 0f;
    float distanceToGround = 0f;
    int heldMail = 0;
    int deliveredMail = 0;
    float dashTimer = 0f;
    float stumbleTimer = 0f;

    public Animator animator;
    public GameObject featherSplosion;
    public Collider runCollider;
    public Collider rollCollider;
    public AudioClip jump;
    public AudioClip flutter;
    public AudioClip roll;
    public AudioClip step;
    public AudioClip hittingTrash;
    public AudioClip hittingManhole;
    public AudioClip hittingCar;
    public AudioClip hittingLady;
    public AudioClip collectingMail;
    public AudioClip sendingMail;
    public Transform checkpoint1;
    public Transform checkpoint2;

    public int HeldMail { get { return heldMail; } }
    public int DeliveredMail { get { return deliveredMail; } }

    void Start ()
    {
        playerInput = ReInput.players.GetSystemPlayer();
        body = GetComponent<Rigidbody>();
        currentSpeed = normalForwardSpeed;
        lerpToSpeed = currentSpeed;
        collider = GetComponent<Collider>();
        audioSource = GetComponent<AudioSource>();

        distanceToGround = collider.bounds.extents.y;
        animator.SetBool("Jumping", false);
    }
	
	void Update ()
    {
        pressingJump = playerInput.GetButtonDown("Jump");
        horizontalMovement = playerInput.GetAxis("Horizontal Movement");
        pressingDash = playerInput.GetButtonDown("Dash");
        holdingSlow = playerInput.GetButton("Slow");
        pressingQuit = playerInput.GetButton("QuitGame");
        pressingRestart = playerInput.GetButton("Restart");

        if (pressingQuit)
        {
            //If we are running in a standalone build of the game
            #if UNITY_STANDALONE
		            //Quit the application
		            Application.Quit();
            #endif

            //If we are running in the editor
            #if UNITY_EDITOR
		            //Stop playing the scene
		            UnityEditor.EditorApplication.isPlaying = false;
            #endif
        }

        if (pressingRestart)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        var wasGrounded = grounded;
        grounded = Physics.Raycast(transform.position, -Vector3.up, distanceToGround + 0.1f);

        if(!wasGrounded && grounded)
        {
            animator.SetBool("Jumping", false);
        }

        if (grounded)
        {
            animator.SetBool("Jumping", false);

            jumpCount = 0;

            lerpToSpeed = normalForwardSpeed;
            if (transform.position.z > checkpoint1.position.z)
                lerpToSpeed = normalForwardSpeedFaster;
            
            if(transform.position.z > checkpoint2.position.z)
                lerpToSpeed = normalForwardSpeedFastest;

            if (pressingDash && dashTimer <= 0 && stumbleTimer <= 0)
            {
                currentSpeed = dashForwardSpeed;
                animator.SetTrigger("Rolling");
                dashTimer = dashTime;

                var random = Random.value;
                if(random < .25f)
                    audioSource.PlayOneShot(roll);
            }
            else if (holdingSlow)
                lerpToSpeed = slowForwardSpeed;

            if (stumbleTimer > 0)
                lerpToSpeed = 0f;
        }

        currentSpeed = Mathf.Lerp(currentSpeed, lerpToSpeed, 2f * Time.deltaTime);
        var movement = new Vector3(horizontalMovement * horizontalSpeed, body.velocity.y, currentSpeed);
        body.velocity = new Vector3(horizontalMovement * horizontalSpeed, body.velocity.y, currentSpeed);

        transform.rotation = Quaternion.LookRotation(new Vector3(movement.x, 0, movement.z));

        if (pressingJump && jumpCount < maxJumpCount && dashTimer <= 0 && stumbleTimer <= 0)
        {
            animator.SetBool("Jumping", true);

            var speed = jumpSpeed;
            if (!grounded)
            {
                speed = flapJumpSpeed;
                animator.SetTrigger("Flapping");
                audioSource.PlayOneShot(flutter);

            }
            else
            {
                var random = Random.value;
                if (random < .25f)
                    audioSource.PlayOneShot(jump);
            }

            body.velocity = new Vector3(body.velocity.x, speed, body.velocity.z);
            jumpCount++;
        }

        if(dashTimer > 0)
        {
            runCollider.enabled = false;
            rollCollider.enabled = true;
        }
        else
        {
            runCollider.enabled = true;
            rollCollider.enabled = false;
        }

        invicibilityTimer -= Time.deltaTime;
        dashTimer -= Time.deltaTime;
        stumbleTimer -= Time.deltaTime;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Obstacle")
        {
            Hurt();

            var obstacle = collision.collider.GetComponent<Obstacle>();
            obstacle.KnockAway(obstacle.transform.position.x > transform.position.x);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Obstacle")
            Hurt();
        else if (collider.tag == "Mailbox")
        {
            SendMail(collider.GetComponent<Mailbox>());
            audioSource.PlayOneShot(sendingMail);
        }
        else if (collider.tag == "Mail")
        {
            audioSource.PlayOneShot(collectingMail);
            Destroy(collider.gameObject);
            CollectMail();
        }
    }

    void Hurt()
    {
        if (invicibilityTimer < 0f)
        {
            invicibilityTimer = invicibilityTime;
            heldMail = 0;
            animator.SetTrigger("Stumble");
            stumbleTimer = stumbleTime;
            var feathers = Instantiate(featherSplosion);
            feathers.transform.position = transform.position;
            audioSource.PlayOneShot(hittingLady);
        }
    }

    void SendMail(Mailbox mailbox)
    {
        if (!mailbox.Sent)
        {
            mailbox.SendMail();
            deliveredMail += heldMail;
            heldMail = 0;
        }
    }

    void CollectMail()
    {
        heldMail++;
    }
}

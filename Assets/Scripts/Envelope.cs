﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Envelope : MonoBehaviour {

	SpriteRenderer spriteRenderer;

    public Sprite envelopeFront;
	public Sprite envelopeBack;
	new GameObject camera;
	private Vector3 startPosition;

	// Use this for initialization
	void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        camera = GameObject.FindGameObjectWithTag("MainCamera");
		startPosition = transform.position;
    }

	// Update is called once per frame
	void Update () {

		transform.Rotate(0,4,0);
		if(Vector3.Dot(transform.forward, (camera.transform.position - transform.position).normalized) < 0)
		{
			spriteRenderer.sprite = envelopeBack;
		}
		else{
			spriteRenderer.sprite = envelopeFront;
		}

		transform.position = startPosition + new Vector3(0.0f, Mathf.Sin(Time.time*3)/4, 0.0f);
	}
}

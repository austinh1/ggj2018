﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mailbox : MonoBehaviour {
    bool retrieved = false;
    SpriteRenderer spriteRenderer;

    public Sprite mailboxDown;

    public bool Sent { get { return retrieved; } set { retrieved = value; } }

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void SendMail()
    {
        retrieved = true;
        spriteRenderer.sprite = mailboxDown;
    }
}
